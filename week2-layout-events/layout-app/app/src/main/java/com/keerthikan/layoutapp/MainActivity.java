package com.keerthikan.layoutapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
    }

    private void setupView() {
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        final EditText etEmailAddress = (EditText) findViewById(R.id.etEmail);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String senderEmail = etEmailAddress.getText().toString();
                if(android.util.Patterns.EMAIL_ADDRESS.matcher(senderEmail).matches()){
                    Intent emailIntent = new Intent(MainActivity.this, EmailActivity.class);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, etEmailAddress.getText().toString());
                    startActivity(emailIntent);
                }else{
                    etEmailAddress.setError("Enter valid email address");
                }

            }
        });
    }
}
