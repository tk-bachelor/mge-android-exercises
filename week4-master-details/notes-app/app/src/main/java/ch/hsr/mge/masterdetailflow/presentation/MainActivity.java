package ch.hsr.mge.masterdetailflow.presentation;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ch.hsr.mge.masterdetailflow.R;

public class MainActivity extends AppCompatActivity implements ItemSelectionListener {

    private boolean isTwoPaneMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (findViewById(R.id.notesDetailContainer) != null) {
            // Wir befinden uns im Tablet-Modus!
            isTwoPaneMode = true;
        }
    }

    @Override
    public void onItemSelected(int position) {
        if (isTwoPaneMode) {
            NoteDetailFragment fragment = NoteDetailFragment.newInstance(position);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.notesDetailContainer, fragment)
                    .commit();
        } else {
            // TODO Aufgabe 2 Activity starten, Parameter nicht vergessen!
            Intent i = new Intent(MainActivity.this, DetailActivity.class);
            i.putExtra(NoteDetailFragment.ARG_ITEM, position);
            startActivity(i);
        }
    }
}
