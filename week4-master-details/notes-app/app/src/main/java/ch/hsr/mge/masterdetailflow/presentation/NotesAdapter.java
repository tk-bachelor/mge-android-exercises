package ch.hsr.mge.masterdetailflow.presentation;

import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import ch.hsr.mge.masterdetailflow.R;
import ch.hsr.mge.masterdetailflow.domain.Note;
import ch.hsr.mge.masterdetailflow.domain.Notes;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    private Notes notes;
    private ItemSelectionListener itemSelectionListener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemRoot, TextView tv) {
            super(itemRoot);
            this.textView = tv;
        }
    }

    public NotesAdapter(Notes notes, ItemSelectionListener selectionListener) {
        this.notes = notes;
        this.itemSelectionListener = selectionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.rowlayout, parent, false);
        TextView textView = v.findViewById(R.id.textView);

        ViewHolder viewHolder = new ViewHolder(v, textView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Note note = notes.get(position);
        holder.textView.setText(note.getTitle());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemSelectionListener.onItemSelected(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.getSize();
    }

}