package com.keerthikan.fragment_wizard.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.keerthikan.fragment_wizard.MainActivity;
import com.keerthikan.fragment_wizard.R;
import com.keerthikan.fragment_wizard.model.UserRegistrationData;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link onStepNewsletterFragmentListner} interface
 * to handle interaction events.
 * Use the {@link StepNewsletterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StepNewsletterFragment extends Fragment {
    private onStepNewsletterFragmentListner mListener;
    private UserRegistrationData userData;

    public StepNewsletterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param userData Parameter 1.
     * @return A new instance of fragment StepNewsletterFragment.
     */
    public static StepNewsletterFragment newInstance(UserRegistrationData userData) {
        StepNewsletterFragment fragment = new StepNewsletterFragment();
        Bundle args = new Bundle();
        args.putSerializable(MainActivity.USER_DATA, userData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userData = (UserRegistrationData) getArguments().getSerializable(MainActivity.USER_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_step_newsletter, container, false);

        TextView tvName = (TextView) rootview.findViewById(R.id.tvRegisteredName);
        tvName.setText(userData.getName());

        Button btnNext = (Button) rootview.findViewById(R.id.newsletterNext);
        final Switch newsletterSwitch = (Switch) rootview.findViewById(R.id.newsletterSwitch);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(newsletterSwitch.isChecked());
            }
        });
        return rootview;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(boolean isSubscribed) {
        if (mListener != null) {
            userData.setNewsletter(isSubscribed);
            mListener.onNewsletterNext(userData);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onStepNewsletterFragmentListner) {
            mListener = (onStepNewsletterFragmentListner) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onStepNewsletterFragmentListner");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface onStepNewsletterFragmentListner {
        void onNewsletterNext(UserRegistrationData userData);
    }
}
